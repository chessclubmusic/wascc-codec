//! # SysInfo Data Types
//!
//! This module contains data types for the `wascc:sys_info` capability provider

use crate::Sample;

/// An operation to retrieve system information from an actor
//pub const OP_SYSINFO: &str = "SysInfo";
pub const OP_GET_BOOT_TIME: &str = "GetBootTime";
pub const OP_GET_NCPU: &str = "GetNCpu";
pub const OP_GET_CPU_SPEED: &str = "GetCpuSPeed";
pub const OP_GET_DISK_INFO: &str = "GetDiskInfo";
pub const OP_GET_LOAD_AVG: &str = "GetLoadAvg";
pub const OP_GET_MEM_INFO: &str = "GetMemInfo";
pub const OP_GET_PROC_TOTAL: &str = "GetProcTotal";


/// Represents a request to retrieve system information. 
#[derive(Debug, PartialEq, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct SysInfoRequest {
    /// A string that represents the encoded system information requested
    pub body: String,
}

impl Sample for SysInfoRequest {
    fn sample() -> Self {
        SysInfoRequest {
            body: "CPU_NUM: 4".to_string(),
        }
    }
}
